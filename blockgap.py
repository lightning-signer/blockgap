#!/bin/env python3

import os
import json
import requests
import collections
from datetime import datetime

rpcPort = 8332
rpcUser = os.environ['RPCUSER']
rpcPassword = os.environ['RPCPASSWORD']
rpcIp = '127.0.0.1'

serverURL = 'http://' + str(rpcIp)+ ":" + str(rpcPort)
headers = {'content-type': 'text/plain'}

# Start of 2016
STARTHEIGHT = 391000
# Start of 2018
#STARTHEIGHT = 502000

# 25% attacking hashrate, 4 hour decision window
THRESHOLD = 6
RECENTSECS = 4 * 60 * 60

def get_starting_block(height):
    payload = json.dumps({"method": 'getblockhash', "params": [height]})
    response = requests.post(serverURL, headers=headers, data=payload, auth=(rpcUser, rpcPassword))
    return response.json()['result']

def get_block_header(blockhash):
    payload = json.dumps({"method": 'getblockheader', "params": [blockhash]})
    response = requests.post(serverURL, headers=headers, data=payload, auth=(rpcUser, rpcPassword))
    return response.json()['result']

recent = collections.deque()
blocktimes = collections.deque()
blockhash = get_starting_block(STARTHEIGHT)
filled = False
while True:
    block = get_block_header(blockhash)
    if 'nextblockhash' not in block:
        break
    blockhash = block['nextblockhash']
    height = block['height']
    blocktime = block['time']
    blocktimes.append(blocktime)

    # Maintain a collection of the last three block times.
    while len(blocktimes) > 3:
        blocktimes.popleft()
    if len(blocktimes) < 3:
        continue

    # Determine the median of the last three block times.
    inorder = list(blocktimes)
    inorder.sort()
    median = inorder[1]
    # if median != blocktimes[1]:
    #     print("{} {} {}".format(height, blocktimes, median))

    # Maintain a window of most recent median blocktimes
    recent.append(median)
    cutoff = median - RECENTSECS
    while len(recent) > 0 and recent[0] < cutoff:
        filled = True
        recent.popleft()

    # Report cases where there aren't many recent blocks.
    if filled and len(recent) <= THRESHOLD:
        date = datetime.fromtimestamp(median)
        print("height:{} median:{} date:{} len(recent):{}".format(height, median, date, len(recent)))
